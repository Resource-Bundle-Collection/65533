# 【解决攻略】StableDiffusion 4.8 中 ADetailer 加载 yolov8x-worldv2.pt 模型失败问题

当您在尝试使用 StableDiffusion 4.8 版本，并启用 ADetailer 插件时，可能会遇到因 Hugging Face 访问限制导致的模型加载错误：“Failed to load model ‘yolov8x-worldv2.pt’ from huggingface”。本 README 将指导您完成故障排除步骤，让您顺利运行 ADetailer。

## 错误详情
错误通常表现为无法从 Hugging Face 下载 yolov8x-worldv2.pt 模型，提示连接超时或被阻止。

## 解决方案概览
1. **手动下载模型**: 由于网络环境或地理限制可能导致的下载失败，我们提供了模型的备用下载方式。
2. **本地配置模型路径**: 修改 ADetailer 插件的源代码，指向您存放模型的本地路径。

### 步骤一：下载模型
- **下载地址**: 模型文件已提供备用下载链接，您需通过有效的下载渠道获取 yolov8x-worldv2.pt 文件。请确保您拥有正确的文件。

### 步骤二：修改代码以使用本地模型
1. **放置模型**: 将下载的 yolov8x-worldv2.pt 文件放到适当的目录，例如 `models\yolo_world_mirror`。
   
2. **定位并编辑代码**:
   - 导航至您的 `stableDiffusion-webui-aki-v4.8/extensions/adetailer/adetailer/common.py` 文件路径。
   
3. **代码修改**:
   - 在 `common.py` 中找到原有的 `get_models()` 函数并注释以防万一，然后添加或修改该函数，确保它可以从您保存 yolov8x-worldv2.pt 的本地目录加载模型。
   - 更新 `local_model_dir` 变量的值为您实际存储模型的路径。
   - 实现或调整代码，使得 `yolov8x-worldv2-pt` 项使用本地路径加载。

### 示例代码段（仅供参考）
```python
def get_models(*dirs: str | os.PathLike[str], huggingface: bool = True) -> OrderedDict[str, str]:
    # ... (其他代码保持不变)
    
    # 假设您的本地模型路径为 'S:/your/path/to/models/yolo_world_mirror'
    local_model_dir = "S:/your/path/to/models/yolo_world_mirror"
    
    models.update([
        # 其他模型的下载或引用...
        "yolov8x-worldv2-pt": get_model_file("yolov8x-worldv2-pt", local_model_dir),
    ])
    # ... (其余部分保持不变)

# 注意：确保替换上述代码中的路径为您的实际本地路径。
```

## 结束语
通过上述步骤，您应该能够成功解决模型加载问题，让您的 StableDiffusion 4.8 项目顺畅运行。记得保存所有修改后的代码，并重新启动您的应用以应用更改。如果遇到任何其他问题，查阅社区讨论或技术支持论坛以寻找额外的帮助。祝您开发愉快！